<?php
declare(strict_types=1);

namespace App\Tests\Service;

use Doctrine\Common\Collections\ArrayCollection;
use App\Importer\QuoteImporter;
use App\Model\Quote;
use App\Service\QuoteSearchService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class QuoteSearchServiceTest extends TestCase
{
    /**
     * @var QuoteSearchService
     */
    private $searchService;

    /**
     * @var QuoteImporter|MockObject
     */
    private $importer;

    public function setUp()
    {
        $this->importer      = $this->createMock(QuoteImporter::class);
        $this->searchService = new QuoteSearchService($this->importer);
    }

    public function testSearchOverLimitThrowsException(): void
    {
        $this->expectException('Exception');
        $this->importer->expects($this->never())
                       ->method('import');
        $this->searchService->search('randomAuthor', 15);
    }

    public function testSearchReturnsResults(): void
    {
        $quoteCollection = $this->prepareQuoteCollection();
        $this->importer
            ->expects($this->once())
            ->method('import')
            ->willReturn($quoteCollection);;

        $result = $this->searchService->search('author2', 10);

        $this->assertCount(3, $result);
    }

    public function testSearchReturnsResultsUntilLimitIsReached(): void
    {
        $quoteCollection = $this->prepareQuoteCollection();
        $this->importer->expects($this->once())
                       ->method('import')
                       ->willReturn($quoteCollection);

        $result = $this->searchService->search('author2', 1);

        $this->assertCount(1, $result);
    }

    private function prepareQuoteCollection(): ArrayCollection
    {
        return new ArrayCollection(
            [
                new Quote('author1', 'quote1'),
                new Quote('author2', 'quote2'),
                new Quote('author2', 'quote3'),
                new Quote('author2', 'quote4'),
                new Quote('author3', 'quote5'),
            ]
        );
    }
}
