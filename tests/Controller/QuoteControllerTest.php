<?php
declare(strict_types=1);

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class QuoteControllerTest extends WebTestCase
{
    public function testSearchReturns200()
    {
        $client = static::createClient();

        $client->request('GET', '/shout/steve-jobs');

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(200);
        $this->assertTrue($client->getResponse()->headers->contains(
            'Content-Type', 'application/json'
        ));
    }

    public function testHarmfulParamReturns404()
    {
        $client = static::createClient();
        $client->request('GET', '/shout/#@!#!@$%');
        $this->assertResponseStatusCodeSame(404);
    }
}
