<?php
declare(strict_types=1);

namespace App\Tests\Importer;

use Doctrine\Common\Collections\ArrayCollection;
use App\Importer\QuoteImporter;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class QuoteImporterTest extends WebTestCase
{
    /**
     * @var QuoteImporter
     */
    private $importer;

    public function setUp()
    {
        $client = static::createClient();

        $this->importer = new QuoteImporter($client->getContainer()->getParameter('kernel.project_dir'));
    }

    public function testImporterReturnsNotEmptyCollection(): void
    {
        $result = $this->importer->import();
        $this->assertInstanceOf(ArrayCollection::class, $result);
        $this->assertGreaterThan(0, count($result));
    }
}
