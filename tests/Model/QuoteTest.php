<?php
declare(strict_types=1);

namespace App\Tests\Model;

use App\Model\Quote;
use PHPUnit\Framework\TestCase;

class QuoteTest extends TestCase
{
    public function testGetContentReturnsString()
    {
        $quote = new Quote('randauthor', 'randquote');
        $this->assertIsString($quote->getContent());
    }

    public function testGetAuthorReturnsString()
    {
        $quote = new Quote('randauthor', 'randquote');
        $this->assertIsString($quote->getAuthor());
    }

    public function testConstructAuthorNotAcceptInt()
    {
        $this->expectException('TypeError');
        $quote = new Quote(15, 'randquote');
        $this->assertIsString($quote->getAuthor());
    }

    public function testConstructContentNotAcceptInt()
    {
        $this->expectException('TypeError');
        $quote = new Quote('randauthor', 15);
        $this->assertIsString($quote->getContent());
    }
}
