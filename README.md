###### What is it? 

Application with REST API endpoint which returns quotes of famous people.

###### How to test? 
1. Run `bin/console server:start`
2. Run `curl -s http://127.0.0.1:8000/shout/steve-jobs?limit=2`
3. If above request doesn't work, try `curl -x "" "http://127.0.0.1:8000/shout/steve-jobs?limit=2"`
