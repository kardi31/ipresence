<?php
declare(strict_types=1);

namespace App\Service;

use App\Importer\ImporterInterface;
use App\Model\Quote;

class QuoteSearchService
{
    private const MAX_SEARCH_LIMIT = 10;

    /**
     * @var ImporterInterface
     */
    private $importer;

    public function __construct(ImporterInterface $importer)
    {
        $this->importer = $importer;
    }

    /**
     * @param string $author
     * @param int $limit
     *
     * @return string[]
     * @throws \Exception
     */
    public function search(string $author, int $limit): array
    {
        if ($limit > self::MAX_SEARCH_LIMIT) {
            throw new \Exception('Results limit has been reached');
        }

        $quotes = $this->importer->import();

        $result = [];
        /** @var Quote $quote */
        foreach ($quotes as $quote) {
            if ($quote->getAuthor() == $author) {
                $formattedContent = $this->formatQuoteContent($quote->getContent());
                array_push($result, $formattedContent);

                if (count($result) >= $limit) {
                    break;
                }
            }
        }

        return $result;
    }

    /**
     * @param string $content
     * @return string
     */
    private function formatQuoteContent(string $content): string
    {
        return sprintf('%s!', mb_strtoupper(rtrim($content, '.!')), 'UTF-8');
    }
}
