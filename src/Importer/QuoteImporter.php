<?php
declare(strict_types=1);

namespace App\Importer;

use App\Model\Quote;
use Doctrine\Common\Collections\ArrayCollection;

class QuoteImporter implements ImporterInterface
{
    /**
     * @var string
     */
    protected $projectPath;

    public function __construct(string $projectPath)
    {
        $this->projectPath = $projectPath;
    }

    /**
     * @return ArrayCollection|<Quote>
     * @throws \Exception
     */
    public function import(): ArrayCollection
    {
        $filepath = sprintf('%s/public/quotes.json', $this->projectPath);
        $content = file_get_contents($filepath);
        if (!$content) {
            throw new \Exception(sprintf('Quotes file is missing in %s', $filepath));
        }

        $parsedJsonContent = json_decode($content);
        if (!$parsedJsonContent || !isset($parsedJsonContent->quotes) || !is_iterable($parsedJsonContent->quotes)) {
            throw new \Exception('Invalid format of quote file');
        }

        $quoteCollection = new ArrayCollection();
        foreach ($parsedJsonContent->quotes as $quoteEntry) {
            $dashedAuthor = preg_replace("/[\s]/", "-", trim($quoteEntry->author));
            if (!$dashedAuthor) {
                continue;
            }
            $sluggedAuthor = strtolower($dashedAuthor);

            $quoteCollection->add(
                new Quote(
                    $sluggedAuthor,
                    $quoteEntry->quote
                )
            );
        }

        return $quoteCollection;
    }
}
