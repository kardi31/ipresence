<?php
declare(strict_types=1);

namespace App\Importer;

use Doctrine\Common\Collections\ArrayCollection;

interface ImporterInterface
{
    /**
     * @return ArrayCollection
     * @throws \Exception
     */
    public function import(): ArrayCollection;
}
