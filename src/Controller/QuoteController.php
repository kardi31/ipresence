<?php
declare(strict_types=1);

namespace App\Controller;

use App\Service\QuoteSearchService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class QuoteController extends AbstractController
{
    /**
     * @var QuoteSearchService
     */
    private $searchService;

    public function __construct(QuoteSearchService $searchService)
    {
        $this->searchService = $searchService;
    }

    public function searchAction(Request $request, string $author): JsonResponse
    {
        $limit = $request->query->has('limit') ? intval($request->query->get('limit')) : 1;

        $result = $this->searchService->search($author, $limit);

        $response = new JsonResponse($result);
        $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);
        $response->setMaxAge(86400); // 1 day
        return $response;
    }
}
