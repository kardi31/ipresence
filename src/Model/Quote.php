<?php
declare(strict_types=1);

namespace App\Model;

class Quote
{
    /**
     * @var string
     */
    private $author;

    /**
     * @var string
     */
    private $content;

    public function __construct(string $author, string $content)
    {
        $this->author = $author;
        $this->content = $content;
    }

    public function getAuthor(): string
    {
        return $this->author;
    }

    public function getContent(): string
    {
        return $this->content;
    }
}
